var app = new Vue({
    el: '#app',
    data : {
        editing: false,
        position:null,
        newUsers: '',
        users:[
            {name: 'Muhammad Iqbal Mubarok'},
            {name: 'Ruby Purwanti'},
            {name: 'Faqih Muhammad'},
        ]
    },
    methods:{
        btnEdit: function(index){
            this.editing= true;
            this.position=index;
            this.newUsers=this.users[index].name;
    },
        btnHapus: function(index){
            if(confirm("anda yakin?")){
                this.users.splice(index,1);
            }
        },
        inputUsers: function(){
            this.users.push({
                "name": this.newUsers,
            });
            this.newUsers='';
        },
        editUsers: function(){
            this.users[this.position].name = this.newUsers;
            this.editing= false;
            this.position= null;
            this.newUsers='';
        }
    }
})