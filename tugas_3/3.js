function Buah(nama, warna, biji, harga){
    this.nama = nama;
    this.warna = warna;
    this.biji = biji;
    this.harga = harga;
}

var strawberry  = new Buah('Strawberry','merah', 'tidak', 5000);
var jeruk       = new Buah('Jeruk','orange', 'ada', 800);
var semangka    = new Buah('Semangka','merah & hijau', 'ada', 10000);
var pisang      = new Buah('Pisang','kuning', 'tidak', 5000);

var tampil = 
            "Nama: "+strawberry.nama+"\r\n"+
            "Warna: "+strawberry.warna+"\r\n"+
            "ada bijinya: "+strawberry.biji+"\r\n"+
            "harga: "+strawberry.harga;

console.log(tampil);