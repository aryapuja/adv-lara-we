Vue.component('dataBaru',{
    template: '#komentar-template',
});

var app = new Vue({
    el: '#app',
    data : {
        newKomentar: '',
        komentar: [
            {isi: "tes 1", tgl: "29-07-2020", vote: 1,hasil: 1,},
            {isi: "tes 2", tgl: "29-07-2020", vote: 2,hasil: 2,},
        ]
    },
    computed:{
        voteNaik: function(){
            return this.vote + 1;
        },
        voteTurun: function(){
            return this.vote - 1;
        }
    },
    methods:{
        prosesNaik: function(index){
            return this.komentar[index].hasil = this.komentar[index].vote + 1;
        },
        prosesTurun: function(index){
            return this.komentar[index].hasil = this.komentar[index].vote - 1;
        },
        inputKomentar: function(){
            this.komentar.push({
                "isi": this.newKomentar,
                tgl: "29-07-2020", 
                vote: 0,
                hasil: 0,
            });
        }
    }
})